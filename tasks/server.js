import gulp from 'gulp';
import browserSync from 'browser-sync';

gulp.task('server', (done) => {
  browserSync({
    server: {
      baseDir: [
        'dist',
      ],
    },
    open: false,
    notify: false,
  });
  done();
});
