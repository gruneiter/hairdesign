const path = require('path');
const webpack = require('webpack');
const ESLintPlugin = require('eslint-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';
const mode = isProduction ? 'production' : 'development';
const jq = new webpack.ProvidePlugin({
  $: 'jquery',
  jQuery: 'jquery',
  'window.jQuery': 'jquery',
});

const plugins = [new ESLintPlugin()];

if (!isProduction) plugins.push(jq);

module.exports = {
  mode,
  entry: {
    main: './src/scripts/script.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: 'script.js',
  },
  devtool: isProduction ? undefined : 'source-map',
  plugins,
  optimization: {
    minimize: isProduction,
  },
};
