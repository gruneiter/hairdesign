import fancybox from '@fancyapps/fancybox'; // eslint-disable-line no-unused-vars
import fotorama from '../../node_modules/fotorama/fotorama'; // eslint-disable-line no-unused-vars
import headerMobileSwitcher from '../blocks/header/header-mobile-switcher/header-mobile-switcher';
import headerNavigation from '../blocks/header/header-navigation/header-navigation';
import showHide from '../blocks/show-hide/show-hide';
import tabs from '../blocks/tabs/tabs';

headerMobileSwitcher();
headerNavigation();
tabs();
showHide();
